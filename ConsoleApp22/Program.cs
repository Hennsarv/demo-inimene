﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp22
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene henn = Inimene.GetInimene("35503070211");
            henn.Nimi = "Henn";
        }
    }

    class Inimene
    {
        private static Dictionary<string, Inimene> Inimesed = new Dictionary<string, Inimene>();

        private string _Isikukood; // see on võti
        private string _Nimi; // see on sisemine nimi

        private Inimene(string isikukood)
        {
            this._Isikukood = isikukood;
            Inimesed.Add(isikukood, this);
        }

        public static Inimene GetInimene(string isikukood)
        {
            if (Inimesed.Keys.Contains(isikukood)) return Inimesed[isikukood];
            else return new Inimene(isikukood);
        }

        public string Nimi
        {
            get => _Nimi;
            set => _Nimi = value.Substring(0, 1).ToUpper() + value.Substring(1).ToLower();
        }

        public DateTime Sünniaeg
        {
            get
            {
                return DateTime.Parse((((int)(_Isikukood[0] - '0') + 35) / 2).ToString() + _Isikukood.Substring(1, 2) + "-" + _Isikukood.Substring(3, 2) + "-" + _Isikukood.Substring(5, 2));
            }
        }

    }


}
